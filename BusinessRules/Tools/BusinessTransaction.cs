using System;

namespace BusinessRules.Tools
{
    public class BusinessTransaction : IBusinessTransaction, IDisposable
    {
        public void Commit()
        {
        }

        public void Rollback()
        {
        }

        public void Dispose()
        {
        }
    }
}