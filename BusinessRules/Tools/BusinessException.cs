﻿using System;
using BusinessRules.Template;

namespace BusinessRules.Tools
{
    public class BusinessException : Exception
    {
        public BusinessException(MessageBase message) : base(message.ToString())
        {
        }

        public BusinessException(string message)
            : base(message)
        {
        }
    }
}