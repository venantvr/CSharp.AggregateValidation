﻿using System;
using BusinessRules.Engine;
using BusinessRules.Engine.Handlers;
using BusinessRules.Engine.Providers;
using BusinessRules.Engine.Template;
using BusinessRules.Enums;

namespace BusinessRules.Tools
{
    public static class BusinessContracts
    {
        private static readonly Func<ExceptionHandler> DefaultFactory = () => new ExceptionHandler();

        [Obsolete]
        public static void Requires(bool condition, string message)
        {
            if (!condition)
            {
                throw new BusinessException(message);
            }
        }

        public static void Requires<TAggregateRoot, TException>(TAggregateRoot entity, RuleLifeCycle preCondition,
            Func<TException> factory) where TException : Handler
        {
            using (
                var bre =
                    new BusinessRuleEngine
                        <AppDomainProvider<TAggregateRoot>, HandlersProvider<TAggregateRoot>,
                            TAggregateRoot>(entity, factory))
            {
                bre.Verify(entity, factory);
            }
        }

        public static void Run<TTransaction, TAggregateRoot>(TAggregateRoot entity, Action<TAggregateRoot> action, Func<Handler> factory = null)
            where TTransaction : IBusinessTransaction, IDisposable, new()
        {
            var effectiveFactory = factory ?? DefaultFactory;

            Requires(entity, RuleLifeCycle.Invariant, effectiveFactory);
            Requires(entity, RuleLifeCycle.PreCondition, effectiveFactory);

            using (var transaction = new TTransaction())
            {
                try
                {
                    action.Invoke(entity);

                    Requires(entity,
                        RuleLifeCycle.PostCondition, effectiveFactory);
                    Requires(entity,
                        RuleLifeCycle.Invariant, effectiveFactory);

                    transaction.Commit();
                }
                    // ReSharper disable once UnusedVariable
                catch (AggregateException exception)
                {
                    var aggregateException = exception.Flatten();

                    transaction.Rollback();

                    throw;
                }
            }
        }
    }
}