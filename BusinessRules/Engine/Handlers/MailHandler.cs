﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Net.Mail;
using BusinessRules.Engine.Template;

namespace BusinessRules.Engine.Handlers
{
    public class MailHandler : Handler
    {
        private const string DefaultMail = "default@mail.com";
        private readonly string _email;

        public MailHandler(string email = null)
        {
            _email = email;
        }

        public string To => _email ?? DefaultMail;

        public override void Commit()
        {
            if (Messages.Any())
            {
                var day = (int) DateTime.Now.DayOfWeek;

                var messages = Messages
                    .Where(m => m != null)
                    .Select(m => $"{m.Level} - {m.Message ?? m.ToString()}").ToList();

                var email = Email
                    .From(DefaultMail)
                    .To(To, To)
                    .Subject($@"{DateTime.Now} - Contrôles")
                    .Body(messages, Email.Colors[day]);

                email.Send();
            }
        }

        public override void DefaultAction()
        {
        }

        public class Email
        {
            public static Color[] Colors =
            {
                Color.Aqua, Color.Cyan, Color.GreenYellow, Color.LightBlue,
                Color.MediumAquamarine, Color.PaleGreen, Color.SkyBlue
            };

            private readonly SmtpClient _client;

            private Email()
            {
                Message = new MailMessage { IsBodyHtml = true };
                _client = new SmtpClient();
            }

            public MailMessage Message { get; set; }

            public static Email From(string emailAddress, string name = "")
            {
                var email = new Email { Message = { From = new MailAddress(emailAddress, name) } };
                return email;
            }

            public Email To(string emailAddress, string name = "")
            {
                Message.To.Add(new MailAddress(emailAddress, name));
                return this;
            }

            public Email Subject(string subject = "")
            {
                Message.Subject = subject;
                return this;
            }

            public Email Body(List<string> body, Color? color = null)
            {
                if (color == null)
                {
                    color = Color.White;
                }

                var output =
                    $"<html><div style=\"background-color: {HexConverter(color.Value)}\">{body.Aggregate(string.Empty, (current, message) => current + $"<br />{message}")}</div></html>";
                Message.Body = output;
                Message.IsBodyHtml = true;
                return this;
            }

            public void Send()
            {
                _client.Send(Message);
            }

            private static string HexConverter(Color c)
            {
                return $"#{c.R.ToString("X2")}{c.G.ToString("X2")}{c.B.ToString("X2")}";
            }
        }
    }
}