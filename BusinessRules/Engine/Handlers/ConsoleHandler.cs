﻿using System;
using System.Linq;
using BusinessRules.Engine.Template;
using BusinessRules.Tools;

namespace BusinessRules.Engine.Handlers
{
    public class ConsoleHandler : Handler
    {
        public override void Commit()
        {
            if (Messages.Any())
            {
                Messages.Where(m => !string.IsNullOrEmpty(m.Message))
                    .ForEach(m => Console.WriteLine(string.Join("\n", m.Message)));
            }
        }

        public override void DefaultAction()
        {
        }
    }
}