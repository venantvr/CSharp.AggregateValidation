using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using BusinessRules.Contracts;
using BusinessRules.Engine.Template;
using BusinessRules.Template;

namespace BusinessRules.Engine
{
    public class BusinessRuleContainer<TRules, THandlers, TAggregateRoot> : IDisposable
        where TRules : IRulesProvider<TAggregateRoot>, IDisposable, new()
        where THandlers : IHandlersProvider<TAggregateRoot>, IDisposable, new()
    {
        private readonly List<Handler> _handlers;
        private readonly THandlers _handlersProvider;
        private readonly List<AbstractBusinessRule<TAggregateRoot>> _rules;
        private readonly TRules _rulesProvider;

        [Obsolete]
        public BusinessRuleContainer(HandlersFactory factories)
        {
            _rulesProvider = new TRules();
            _handlersProvider = new THandlers();

            _rules = _rulesProvider.Resolve().ToList();
            _handlers = _handlersProvider.Load(_rules).Resolve(factories);
        }

        [Obsolete]
        public BusinessRuleContainer(params Func<Handler>[] factories)
        {
            _rulesProvider = new TRules();
            _handlersProvider = new THandlers();

            _rules = _rulesProvider.Resolve().ToList();
            _handlers = _handlersProvider.Load(_rules).Resolve(factories);
        }

        public BusinessRuleContainer(TAggregateRoot aggregateRoot, params Func<Handler>[] factories)
        {
            _rulesProvider = new TRules();
            _handlersProvider = new THandlers();

            _rules = _rulesProvider.Resolve(aggregateRoot).ToList();
            _handlers = _handlersProvider.Load(_rules).Resolve(factories);
        }

        public ReadOnlyCollection<AbstractBusinessRule<TAggregateRoot>> Rules => _rules.AsReadOnly();

        public ReadOnlyCollection<Handler> Handlers => _handlers.AsReadOnly();

        public void Dispose()
        {
            _handlersProvider.Dispose();
            _rulesProvider.Dispose();
        }

        public IEnumerable<AbstractBusinessRule<TAggregateRoot>> RulesApplyingTo(TAggregateRoot entity)
        {
            var businessRuleFilter = new BusinessRuleFilter(Rules);

            return businessRuleFilter.Filter().Cast<AbstractBusinessRule<TAggregateRoot>>();
        }

        private class BusinessRuleFilter
        {
            private readonly ReadOnlyCollection<AbstractBusinessRule<TAggregateRoot>> _rules;

            public BusinessRuleFilter(ReadOnlyCollection<AbstractBusinessRule<TAggregateRoot>> rules)
            {
                _rules = rules;
            }

            public IEnumerable<AbstractBusinessRule<TAggregateRoot>> Filter()
            {
                return from r in _rules where r.RootType == typeof (TAggregateRoot) select r;
            }
        }
    }
}