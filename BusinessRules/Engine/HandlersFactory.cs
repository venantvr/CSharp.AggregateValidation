﻿using System;
using System.Collections.Generic;
using BusinessRules.Engine.Template;

namespace BusinessRules.Engine
{
    public class HandlersFactory
    {
        private readonly Dictionary<Type, Func<Handler>> _factories;

        public HandlersFactory()
        {
            _factories = new Dictionary<Type, Func<Handler>>();
        }

        internal Func<Handler> GetFactory(Type type)
        {
            return _factories[type];
        }

        internal void Add(Type type, Func<Handler> factory)
        {
            if (_factories.ContainsKey(type))
            {
                throw new ArgumentException(@"The type is always defined...");
            }

            _factories.Add(type, factory);
        }

        //internal void Add<T>(Func<Notifier> factory) where T : Notifier
        //{
        //    Add(typeof (T), factory);
        //}

        internal void Add<T>(Func<T> factory) where T : Handler
        {
            Add(typeof (T), factory);
        }
    }
}