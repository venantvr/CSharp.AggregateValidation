﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Runtime.CompilerServices;
using BusinessRules.Engine.Template;
using BusinessRules.Tools;

[assembly: InternalsVisibleTo("BusinessRules.Tests")]

namespace BusinessRules.Engine.Logger
{
    internal class LoggerOptions : IDisposable
    {
        private static readonly Stack<LoggerOptions> ScopeStack = new Stack<LoggerOptions>();
        private readonly bool _parentExists = ScopeStack.Count > 0;

        public LoggerOptions(ReadOnlyCollection<Handler> loggers)
        {
            Horodatage = _parentExists ? Current.Horodatage : DateTime.Now;

            Loggers = loggers ?? new List<Handler>().AsReadOnly();

            ScopeStack.Push(this);
        }

        public DateTime Horodatage { get; }

        public static LoggerOptions Current
        {
            get
            {
                if (ScopeStack.Count == 0)
                {
                    ScopeStack.Push(new LoggerOptions(new ReadOnlyCollection<Handler>(new List<Handler>())));
                }

                return ScopeStack.Peek();
            }
        }

        public ReadOnlyCollection<Handler> Loggers { get; }

        #region IDisposable Members

        public void Dispose()
        {
            if (ShouldUnwindScope())
            {
                UnwindScope();
            }

            var item = ScopeStack.Pop();

            item.Loggers.ForEach(l => l.Dispose());
        }

        #endregion

        private void UnwindScope()
        {
        }

        private bool ShouldUnwindScope()
        {
            return true;
        }
    }
}