﻿using System;
using BusinessRules.Engine.Template;
using BusinessRules.Enums;

namespace BusinessRules.Engine
{
    // TODO : à instancier plusieurs fois sur une même classe, etc...
    public class BusinessRuleAttribute : Attribute
    {
        public BusinessRuleAttribute(string description, RuleLifeCycle ruleLifeCycle, CriticityLevel criticityLevel,
            Type handlerType)
        {
            if (!handlerType.IsSubclassOf(typeof (Handler)))
            {
                throw new ArgumentException("Handlers should inherit from the \"Handler\" abstract class...");
            }

            Description = description;
            RuleLifeCycle = ruleLifeCycle;
            HandlerType = handlerType;
            CriticityLevel = criticityLevel;
        }

        public string Description { get; }
        public Type HandlerType { get; }
        public RuleLifeCycle RuleLifeCycle { get; }
        public CriticityLevel CriticityLevel { get; }
    }
}