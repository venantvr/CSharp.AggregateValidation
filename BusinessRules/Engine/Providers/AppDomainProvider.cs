using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Linq;
using BusinessRules.Contracts;
using BusinessRules.Template;

namespace BusinessRules.Engine.Providers
{
    public class AppDomainProvider<TAggregateRoot> : IRulesProvider<TAggregateRoot> // where TAggregateRoot : new()
    {
        private static readonly Lazy<List<Type>> Targets = new Lazy<List<Type>>(() =>
                                                                                {
                                                                                    var types = new List<Type>();

                                                                                    var assemblies = AppDomain.CurrentDomain.GetAssemblies();

                                                                                    foreach (var assembly in assemblies)
                                                                                    {
                                                                                        var classes =
                                                                                            assembly.GetTypes()
                                                                                                .Where(t => t.IsClass && !t.IsAbstract && t.IsSubclassOf(typeof (AbstractBusinessRule<TAggregateRoot>)))
                                                                                                .ToList();

                                                                                        if (classes.Any())
                                                                                        {
                                                                                            types.AddRange(classes);
                                                                                        }
                                                                                    }

                                                                                    return types;
                                                                                });

        public void Dispose()
        {
        }

        public ConcurrentBag<AbstractBusinessRule<TAggregateRoot>> Resolve()
        {
            var rules = new ConcurrentBag<AbstractBusinessRule<TAggregateRoot>>();

            if (Targets.Value.Any())
            {
                foreach (
                    var target in Targets.Value.Select(Activator.CreateInstance).Cast<AbstractBusinessRule<TAggregateRoot>>())
                {
                    rules.Add(target);
                }
            }

            return rules;
        }

        public ConcurrentBag<AbstractBusinessRule<TAggregateRoot>> Resolve(TAggregateRoot aggregateRoot)
        {
            var rules = new ConcurrentBag<AbstractBusinessRule<TAggregateRoot>>();

            if (Targets.Value.Any())
            {
                foreach (
                    var target in
                        Targets.Value
                            .Where(t => t.GetConstructors()
                                .Any(
                                    c =>
                                    {
                                        var parameterInfos = c.GetParameters();
                                        return parameterInfos.Length == 1 &&
                                               parameterInfos.Count(p => p.ParameterType == typeof (TAggregateRoot)) == 1;
                                    }))
                            .Select(t => Activator.CreateInstance(t, aggregateRoot))
                            .Cast<AbstractBusinessRule<TAggregateRoot>>())
                {
                    rules.Add(target);
                }
            }

            return rules;
        }
    }
}