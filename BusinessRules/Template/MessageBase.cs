﻿using BusinessRules.Contracts;
using BusinessRules.Enums;

namespace BusinessRules.Template
{
    public class MessageBase : IMessageBase
    {
        public MessageBase(string message, CriticityLevel level)
        {
            Message = message;
            Level = level;
        }

        public string Message { get; }
        public CriticityLevel Level { get; }

        public new string ToString()
        {
            return $"{Level} - {Message}";
        }
    }
}