using System;
using System.Collections.Generic;
using System.Linq;
using BusinessRules.Engine.Logger;
using BusinessRules.Engine.Template;
using BusinessRules.Tools;

namespace BusinessRules.Template
{
    public abstract class AbstractBusinessRule<TAggregateRoot>
    {
        public readonly DateTime Horodatage = DateTime.Now;

        public abstract string Key { get; }

        public Type RootType => typeof (TAggregateRoot);

        [Obsolete]
        public TAggregateRoot AggregateRoot { get; private set; }

        [Obsolete]
        public AbstractBusinessRule<TAggregateRoot> Load(TAggregateRoot aggregateRoot)
        {
            AggregateRoot = aggregateRoot;

            return this;
        }

        public void Log()
        {
        }

        [Obsolete]
        public AbstractBusinessRule<TAggregateRoot> Verify()
        {
            var issues = this.ExecuteTest(new[] { AggregateRoot });

            foreach (var computed in issues)
            {
                LoggerOptions.Current.Loggers
                    .Where(l => l.GetType() == computed.HandlerType)
                    // ReSharper disable once RedundantAssignment
                    .ForEach(m => m += computed.Message);
            }

            return this;
        }

        public AbstractBusinessRule<TAggregateRoot> Verify(TAggregateRoot entity)
        {
            var issues = this.ExecuteTest(new[] { entity });

            foreach (var computed in issues)
            {
                LoggerOptions.Current.Loggers
                    .Where(l => l.GetType() == computed.HandlerType)
                    // ReSharper disable once RedundantAssignment
                    .ForEach(m => m += computed.Message);
            }

            return this;
        }

        public AbstractBusinessRule<TAggregateRoot> Verify(TAggregateRoot entity, IEnumerable<Handler> instances)
        {
            var issues = this.ExecuteTest(new[] { entity });

            foreach (var computed in issues)
            {
                // ReSharper disable once PossibleMultipleEnumeration
                instances.ForEach(m => m += computed.Message);
            }

            return this;
        }
    }
}