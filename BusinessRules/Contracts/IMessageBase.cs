﻿using BusinessRules.Enums;

namespace BusinessRules.Contracts
{
    public interface IMessageBase
    {
        string Message { get; }
        CriticityLevel Level { get; }
    }
}