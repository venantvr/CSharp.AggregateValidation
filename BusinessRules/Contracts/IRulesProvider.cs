using System;
using System.Collections.Concurrent;
using BusinessRules.Template;

namespace BusinessRules.Contracts
{
    public interface IRulesProvider<TAggregateRoot> : IDisposable // where TAggregateRoot : new()
    {
        ConcurrentBag<AbstractBusinessRule<TAggregateRoot>> Resolve();

        ConcurrentBag<AbstractBusinessRule<TAggregateRoot>> Resolve(TAggregateRoot aggregateRoot);
    }
}