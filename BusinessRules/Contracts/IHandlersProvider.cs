using System;
using System.Collections.Generic;
using BusinessRules.Engine;
using BusinessRules.Engine.Template;
using BusinessRules.Template;

namespace BusinessRules.Contracts
{
    public interface IHandlersProvider<TAggregateRoot> : IDisposable // where TAggregateRoot : new()
    {
        List<Handler> Resolve(HandlersFactory factories);

        IHandlersProvider<TAggregateRoot> Load(List<AbstractBusinessRule<TAggregateRoot>> rules);

        List<Handler> Resolve<THandler>(Func<THandler>[] factories) where THandler : Handler;
    }
}