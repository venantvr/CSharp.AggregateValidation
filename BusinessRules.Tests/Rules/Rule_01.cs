using BusinessRules.Engine;
using BusinessRules.Engine.Handlers;
using BusinessRules.Enums;
using BusinessRules.Template;
using BusinessRules.Tests.Entities;
using BusinessRules.Tests.Handlers;

namespace BusinessRules.Tests.Rules
{
    // ReSharper disable once InconsistentNaming
    public class Rule_01 : AbstractBusinessRule<Entity_01>
    {
        public override string Key => AggregateRoot.Id.ToString();

        [BusinessRule("Test : {0}", RuleLifeCycle.Invariant, CriticityLevel.Error, typeof (ConsoleHandler))]
        public bool Check_01()
        {
            return false;
        }

        [BusinessRule("Test : {0}", RuleLifeCycle.Invariant, CriticityLevel.Error, typeof (SnapshotHandler))]
        public bool Check_02()
        {
            return false;
        }
    }
}