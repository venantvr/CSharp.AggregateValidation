using BusinessRules.Engine;
using BusinessRules.Enums;
using BusinessRules.Template;
using BusinessRules.Tests.Entities;
using BusinessRules.Tests.Handlers;

namespace BusinessRules.Tests.Rules
{
    // ReSharper disable once InconsistentNaming

    public class SampleRule : AbstractBusinessRule<SampleAggregateRoot>
    {
        private readonly SampleAggregateRoot _aggregateRoot;

        public SampleRule(SampleAggregateRoot aggregateRoot)
        {
            _aggregateRoot = aggregateRoot;
        }

        public override string Key => _aggregateRoot.Id.ToString();

        [BusinessRule("The price of product {0} should be positive.", RuleLifeCycle.Invariant, CriticityLevel.Error,
            typeof (SnapshotHandler))]
        public bool CheckPrice()
        {
            return _aggregateRoot.Price > 0.0M;
        }
    }
}