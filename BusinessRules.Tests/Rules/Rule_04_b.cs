using BusinessRules.Engine;
using BusinessRules.Enums;
using BusinessRules.Template;
using BusinessRules.Tests.Entities;
using BusinessRules.Tests.Handlers;

namespace BusinessRules.Tests.Rules
{
    public class Rule_04_b : AbstractBusinessRule<Entity_04>
    {
        private readonly Entity_04 _aggregateRoot;

        //public Rule_03()
        //{
        //}

        public Rule_04_b(Entity_04 aggregateRoot)
        {
            _aggregateRoot = aggregateRoot;
        }

        public override string Key => _aggregateRoot.Id.ToString();

        [BusinessRule("Test : {0}", RuleLifeCycle.Invariant, CriticityLevel.Error, typeof (SnapshotHandler))]
        public bool Check_01()
        {
            return false;
        }

        [BusinessRule("Test : {0}", RuleLifeCycle.Invariant, CriticityLevel.Error, typeof (SnapshotHandler))]
        public bool Check_02()
        {
            return false;
        }
    }
}