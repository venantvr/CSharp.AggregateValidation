﻿using BusinessRules.Engine;
using BusinessRules.Engine.Handlers;
using BusinessRules.Engine.Providers;
using BusinessRules.Tests.Entities;
using BusinessRules.Tests.Handlers;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace BusinessRules.Tests
{
    /// <summary>
    ///     Summary description for BusinessRuleEngineTest
    /// </summary>
    [TestClass]
    public class BusinessRuleEngineTest
    {
        /// <summary>
        ///     Gets or sets the test context which provides
        ///     information about and functionality for the current test run.
        /// </summary>
        public TestContext TestContext { get; set; }

        [TestMethod]
        public void TestBusinessRuleEngine_01()
        {
            var a = new Entity_01();

            using (
                var bre =
#pragma warning disable 612
                    new BusinessRuleEngine<AppDomainProvider<Entity_01>, HandlersProvider<Entity_01>, Entity_01>(
#pragma warning restore 612
                        () => new ConsoleHandler(), () => new SnapshotHandler()))
            {
                bre.Verify(a);
            }

            Assert.IsTrue(SnapshotHandler.NbMessages == 1);
        }

        [TestMethod]
        public void TestBusinessRuleEngine_02()
        {
            var a = new Entity_02();

            using (
                var bre =
#pragma warning disable 612
                    new BusinessRuleEngine<AppDomainProvider<Entity_02>, HandlersProvider<Entity_02>, Entity_02>(
#pragma warning restore 612
                        () => new ConsoleHandler(), () => new SnapshotHandler()))
            {
                bre.Verify(a);
            }

            Assert.IsTrue(SnapshotHandler.NbMessages == 0);
        }

        [TestMethod]
        public void TestBusinessRuleEngine_03()
        {
            var a = new Entity_03();

            using (
                var bre =
                    new BusinessRuleEngine<AppDomainProvider<Entity_03>, HandlersProvider<Entity_03>, Entity_03>(a,
                        () => new ConsoleHandler(), () => new SnapshotHandler()))
            {
                bre.Verify(a);
            }

            Assert.IsTrue(SnapshotHandler.NbMessages == 2);
        }

        [TestMethod]
        public void TestBusinessRuleEngine_04()
        {
            var aggregateRoot = new SampleAggregateRoot();

            using (
                var bre =
                    new BusinessRuleEngine
                        <AppDomainProvider<SampleAggregateRoot>, HandlersProvider<SampleAggregateRoot>,
                            SampleAggregateRoot>(aggregateRoot,
                                () => new ConsoleHandler(), () => new MailHandler()))
            {
                bre.Verify(aggregateRoot);
            }

            // Assert...
        }

        #region Additional test attributes

        //
        // You can use the following additional attributes as you write your tests:
        //
        // Use ClassInitialize to run code before running the first test in the class
        // [ClassInitialize()]
        // public static void MyClassInitialize(TestContext testContext) { }
        //
        // Use ClassCleanup to run code after all tests in a class have run
        // [ClassCleanup()]
        // public static void MyClassCleanup() { }
        //
        // Use TestInitialize to run code before running each test 
        // [TestInitialize()]
        // public void MyTestInitialize() { }
        //
        // Use TestCleanup to run code after each test has run
        // [TestCleanup()]
        // public void MyTestCleanup() { }
        //

        #endregion
    }
}