using System.Collections.Generic;
using BusinessRules.Engine.Template;
using BusinessRules.Tools;

namespace BusinessRules.Tests.Handlers
{
    public class TestHandler : Handler
    {
        private readonly List<string> _externalMessages;

        public TestHandler(List<string> externalMessages)
        {
            _externalMessages = externalMessages;
        }

        public override void Commit()
        {
            Messages.ForEach(m => _externalMessages.Add(m.Message));
        }

        public override void DefaultAction()
        {
        }
    }
}