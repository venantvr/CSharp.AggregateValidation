using BusinessRules.Engine.Template;

namespace BusinessRules.Tests.Handlers
{
    public class SnapshotHandler : Handler
    {
        public static int NbMessages { get; set; }

        public override void Commit()
        {
            NbMessages = Messages.Count;
        }

        public override void DefaultAction()
        {
        }
    }
}